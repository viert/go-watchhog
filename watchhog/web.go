package watchhog

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func (wh *WatchHog) summaryHandler(w http.ResponseWriter, r *http.Request) {
	msg := `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Summary</title>
</head>
<body>
	<h3>Endpoints</h3>
	<ul>
%s
	</ul>
</body>
</html>`
	endpoints := `<li><a href="/metrics">/metrics</a></li>` + "\n"
	for _, c := range wh.collectors {
		endpoints += fmt.Sprintf(`<li><a href="/raw/%s">/raw/%s</a></li>`+"\n", c.Name(), c.Name())
	}

	w.Header().Set("Content-Type", "text/html")
	w.Write([]byte(fmt.Sprintf(msg, endpoints)))
}

func (wh *WatchHog) rawDataHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	c, found := wh.collectors[vars["collectorName"]]
	if !found {
		http.NotFound(w, r)
		return
	}

	data, err := c.RawDataJSON()
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(err.Error()))
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(data)
}
