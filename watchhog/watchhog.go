package watchhog

import (
	"log"
	"math/rand"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/viert/go-watchhog/collector"
	"github.com/viert/go-watchhog/config"
)

// WatchHog is the main watchhog server object
type WatchHog struct {
	server     *http.Server
	collectors map[string]*collector.Collector
}

// New creates a new WatchHog instance
func New(configFilename string) (*WatchHog, error) {

	rand.Seed(time.Now().UnixNano())

	wc, err := config.Load(configFilename)
	if err != nil {
		return nil, err
	}

	cMap := make(map[string]*collector.Collector)
	for name, cc := range wc.Collectors {
		coll, err := collector.New(cc)
		if err != nil {
			return nil, err
		}
		cMap[name] = coll
	}

	wh := &WatchHog{
		collectors: cMap,
	}

	r := mux.NewRouter()
	r.HandleFunc("/", wh.summaryHandler)
	r.HandleFunc("/raw/{collectorName}", wh.rawDataHandler)
	r.Handle("/metrics", promhttp.Handler())

	wh.server = &http.Server{
		Addr:    wc.Listen,
		Handler: r,
	}

	return wh, nil
}

// Start starts all the collectors and the web server
func (wh *WatchHog) Start() {
	for _, coll := range wh.collectors {
		coll.Start()
	}
	go func() {
		log.Println("HTTP server is starting")
		err := wh.server.ListenAndServe()
		if err != nil {
			log.Println(err)
		}
	}()
}

// Stop stops all the collectors and the web server
func (wh *WatchHog) Stop() error {
	for _, coll := range wh.collectors {
		coll.Stop()
	}
	return wh.server.Shutdown(nil)
}
