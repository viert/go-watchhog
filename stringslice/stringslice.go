package stringslice

// Index returns index of an item in the given slice
func Index(item string, slice []string) int {
	for i := 0; i < len(slice); i++ {
		if item == slice[i] {
			return i
		}
	}
	return -1
}

// Contains checks if item is in the given slice
func Contains(item string, slice []string) bool {
	return Index(item, slice) >= 0
}
