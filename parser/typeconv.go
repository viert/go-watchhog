package parser

import (
	"reflect"
	"strconv"
	"strings"
	"time"
)

var dtFormatMap = map[string]string{
	"%Y": "2006",
	"%m": "01",
	"%d": "02",
	"%H": "15",
	"%M": "04",
	"%S": "05",
	"%a": "Mon",
	"%A": "Monday",
	"%b": "Jan",
	"%B": "January",
	"%y": "06",
	"%f": "000",
}

func convertInt(strValue string, f reflect.Value) error {
	x, err := strconv.ParseInt(strValue, 10, 64)
	if err != nil {
		return err
	}
	f.SetInt(x)
	return nil
}

func convertUint(strValue string, f reflect.Value) error {
	x, err := strconv.ParseUint(strValue, 10, 64)
	if err != nil {
		return err
	}
	f.SetUint(x)
	return nil
}

func convertFloat(strValue string, f reflect.Value) error {
	x, err := strconv.ParseFloat(strValue, 64)
	if err != nil {
		return err
	}
	f.SetFloat(x)
	return nil
}

func makeTimeConvert(format string) typeconvFunc {
	for from, to := range dtFormatMap {
		format = strings.ReplaceAll(format, from, to)
	}
	// TODO check and convert datetime format
	return func(strValue string, f reflect.Value) error {
		x, err := time.Parse(format, strValue)
		if err != nil {
			return err
		}
		f.Set(reflect.ValueOf(x))
		return nil
	}
}
