package parser

import (
	"encoding/json"
	"reflect"
	"testing"
	"time"
)

var (
	data interface{}
)

func TestParseString(t *testing.T) {
	var err error
	const testCase1 = "[brackets] \"some data in quotes\" 300\n"
	expected := []string{"brackets", "some data in quotes", "300"}

	parser := NewParser()
	err = parser.Skip('[')
	if err != nil {
		t.Errorf("error adding rule: %v", err)
		return
	}
	err = parser.UpTo(']', "brackets", reflect.TypeOf(""), nil)
	if err != nil {
		t.Errorf("error adding rule: %v", err)
		return
	}
	err = parser.Skip(']')
	if err != nil {
		t.Errorf("error adding rule: %v", err)
		return
	}
	err = parser.Skip(' ')
	if err != nil {
		t.Errorf("error adding rule: %v", err)
		return
	}
	err = parser.Skip('"')
	if err != nil {
		t.Errorf("error adding rule: %v", err)
		return
	}
	err = parser.UpTo('"', "quotes", reflect.TypeOf(""), nil)
	if err != nil {
		t.Errorf("error adding rule: %v", err)
		return
	}
	err = parser.Skip('"')
	if err != nil {
		t.Errorf("error adding rule: %v", err)
		return
	}
	err = parser.Skip(' ')
	if err != nil {
		t.Errorf("error adding rule: %v", err)
		return
	}
	err = parser.UpTo('\n', "number", reflect.TypeOf(""), nil)
	if err != nil {
		t.Errorf("error adding rule: %v", err)
		return
	}
	err = parser.Compile()
	if err != nil {
		t.Errorf("error compiling: %v", err)
		return
	}

	data, err := parser.ParseLine(testCase1)
	if err != nil {
		t.Errorf("error parsing: %v", err)
		return
	}

	_, err = json.Marshal(data)
	if err != nil {
		t.Errorf("error marshalling json: %v", err)
		return
	}

	v := reflect.ValueOf(data).Elem()
	if v.Kind() != reflect.Struct {
		t.Errorf("result kind should be struct. got %v", v.Kind())
		return
	}

	if v.NumField() != len(expected) {
		t.Errorf("result struct has to contain exactly %d fields, got %d", len(expected), v.NumField())
		return
	}

	for i := 0; i < len(expected); i++ {
		if expected[i] != v.Field(i).String() {
			t.Errorf("invalid result in field %d: expected %s, got %s", i, expected[i], v.Field(i).String())
		}
	}
}

func TestIntParse(t *testing.T) {
	const testCase = "hello -35 12384\n"
	p := NewParser()
	p.UpTo(' ', "string", reflect.TypeOf(""), nil)
	p.FromTo(' ', ' ', "signed", reflect.TypeOf(int64(0)), convertInt)
	p.UpTo('\n', "unsigned", reflect.TypeOf(uint64(0)), convertUint)
	p.Compile()
	data, err := p.ParseLine(testCase)
	if err != nil {
		t.Errorf("%v", err)
	}

	str := reflect.ValueOf(data).Elem().Field(0).String()
	if str != "hello" {
		t.Errorf("string field invalid. expected \"string\", got \"%s\"", str)
	}

	sign := reflect.ValueOf(data).Elem().Field(1).Int()
	if sign != -35 {
		t.Errorf("signed field invalid. expected -35, got %d", sign)
	}

	unsign := reflect.ValueOf(data).Elem().Field(2).Uint()
	if unsign != 12384 {
		t.Errorf("unsigned field invalid. expected 12384, got %d", unsign)
	}
}

func TestPatternConfiguration(t *testing.T) {
	p, err := FromPattern(`[$string] $signed_number:int ${unsigned_number:uint} (${a_string}:int) [${date:dt(%d/%b/%Y:%H:%M:%S)}]`)
	if err != nil {
		t.Errorf("error configuring parser: %v", err)
		return
	}

	data, err = p.ParseLine("[a string] -35 498 (some sort of misconfig:int) [24/Aug/2019:10:45:34]\n")
	if err != nil {
		t.Errorf("error parsing data: %v", err)
	}

	item := reflect.ValueOf(data)

	stringField := item.Elem().FieldByName("String")
	if stringField.String() != "a string" {
		t.Errorf("field string should contain \"a string\", got \"%s\"", stringField.String())
	}

	intField := item.Elem().FieldByName("Signed_number")
	if intField.Int() != -35 {
		t.Errorf("field signed_number should contain -35, got %d", intField.Int())
	}

	uintField := item.Elem().FieldByName("Unsigned_number")
	if uintField.Uint() != 498 {
		t.Errorf("field unsigned_number should contain 498, got %d", uintField.Uint())
	}

	aStringField := item.Elem().FieldByName("A_string")
	if aStringField.String() != "some sort of misconfig" {
		t.Errorf("field string should contain \"some sort of misconfig\", got \"%s\"", aStringField.String())
	}

	dtField := item.Elem().FieldByName("Date")
	dt, ok := dtField.Interface().(time.Time)
	if !ok {
		t.Errorf("not a time type")
	}

	if dt.Year() != 2019 {
		t.Errorf("dt year should be 2019, got %d", dt.Year())
	}

	if dt.Month() != 8 {
		t.Errorf("dt month should be 8, got %d", dt.Month())
	}

	if dt.Day() != 24 {
		t.Errorf("dt day should be 24, got %d", dt.Day())
	}

	if dt.Hour() != 10 {
		t.Errorf("dt hour should be 10, got %d", dt.Hour())
	}

}

func BenchmarkParser1(b *testing.B) {
	const testCase1 = "[brackets] \"some data in quotes\" 300\n"

	parser := NewParser()
	parser.Skip('[')
	parser.UpTo(']', "brackets", reflect.TypeOf(""), nil)
	parser.Skip(']')
	parser.Skip(' ')
	parser.Skip('"')
	parser.UpTo('"', "quotes", reflect.TypeOf(""), nil)
	parser.Skip('"')
	parser.Skip(' ')
	parser.UpTo('\n', "number", reflect.TypeOf(""), nil)
	parser.Compile()

	for i := 0; i < b.N; i++ {
		data, _ = parser.ParseLine(testCase1)
	}
}

func BenchmarkParser2(b *testing.B) {
	const testCase1 = "[brackets] \"some data in quotes\" 300\n"

	parser := NewParser()
	parser.FromTo('[', ']', "brackets", reflect.TypeOf(""), nil)
	parser.Skip(' ')
	parser.FromTo('"', '"', "quotes", reflect.TypeOf(""), nil)
	parser.Skip(' ')
	parser.UpTo('\n', "number", reflect.TypeOf(""), nil)
	parser.Compile()

	for i := 0; i < b.N; i++ {
		data, _ = parser.ParseLine(testCase1)
	}
}

func BenchmarkParserConv(b *testing.B) {
	const testCase1 = "[error] \"-958\" 500\n"

	parser := NewParser()
	parser.FromTo('[', ']', "level", reflect.TypeOf(""), nil)
	parser.Skip(' ')
	parser.FromTo('"', '"', "signed", reflect.TypeOf(int64(0)), convertInt)
	parser.Skip(' ')
	parser.UpTo('\n', "status", reflect.TypeOf(uint64(0)), convertUint)
	parser.Compile()

	for i := 0; i < b.N; i++ {
		data, _ = parser.ParseLine(testCase1)
	}
}
