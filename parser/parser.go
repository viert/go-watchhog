package parser

import (
	"fmt"
	"reflect"
	"regexp"
	"strings"
)

type operand int

var (
	validFieldName = regexp.MustCompile(`^[a-zA-Z_][_a-zA-Z0-9]*$`)
)

const (
	opSkip operand = iota
	opSkipTo
	opSkipAny
	opFromTo
	opUpTo
)

type instruction struct {
	op   operand
	arg1 rune
	arg2 rune
}

func (i instruction) String() string {
	var str string
	switch i.op {
	case opSkip:
		str = "Skip    "
	case opSkipTo:
		str = "SkipTo  "
	case opSkipAny:
		str = "SKipAny "
	case opFromTo:
		str = "FromTo  "
	case opUpTo:
		str = "UpTo    "
	}
	str += fmt.Sprintf(" \"%s\"(0x%4x) \"%s\"(0x%4x) ", string(i.arg1), i.arg1, string(i.arg2), i.arg2)
	return str
}

type typeconvFunc func(string, reflect.Value) error

type fieldDesc struct {
	fieldName string
	fieldType reflect.Type
	conv      typeconvFunc
}

// Parser represents a parser object
type Parser struct {
	program    []instruction
	fields     []fieldDesc
	resultType reflect.Type
}

// NewParser creates a new parser
func NewParser() *Parser {
	return &Parser{[]instruction{}, []fieldDesc{}, nil}
}

// UpTo creates a rule making the parser read up to the given symbol
func (p *Parser) UpTo(sym rune, fieldName string, fieldType reflect.Type, converter typeconvFunc) error {
	if p.resultType != nil {
		return fmt.Errorf("can't add rules to already compiled parser")
	}

	if !validFieldName.MatchString(fieldName) {
		return fmt.Errorf("invalid field name \"%s\"", fieldName)
	}

	i := instruction{
		op:   opUpTo,
		arg1: sym,
	}
	p.program = append(p.program, i)
	p.fields = append(p.fields, fieldDesc{fieldName, fieldType, converter})
	return nil
}

// FromTo creates a rule making the parser read from the given symbol up to the given symbol
// excluding them both from the output
func (p *Parser) FromTo(from rune, to rune, fieldName string, fieldType reflect.Type, converter typeconvFunc) error {
	if p.resultType != nil {
		return fmt.Errorf("can't add rules to already compiled parser")
	}

	if !validFieldName.MatchString(fieldName) {
		return fmt.Errorf("invalid field name \"%s\"", fieldName)
	}

	i := instruction{
		op:   opFromTo,
		arg1: from,
		arg2: to,
	}
	p.program = append(p.program, i)
	p.fields = append(p.fields, fieldDesc{fieldName, fieldType, converter})
	return nil
}

// SkipTo creates a rule for skipping symbols up to given one
func (p *Parser) SkipTo(sym rune) error {
	if p.resultType != nil {
		return fmt.Errorf("can't add rules to already compiled parser")
	}

	i := instruction{
		op:   opSkipTo,
		arg1: sym,
	}
	p.program = append(p.program, i)
	return nil
}

// Skip creates a rule for skipping one (given) symbol
func (p *Parser) Skip(sym rune) error {
	if p.resultType != nil {
		return fmt.Errorf("can't add rules to already compiled parser")
	}

	i := instruction{
		op:   opSkip,
		arg1: sym,
	}
	p.program = append(p.program, i)
	return nil
}

// SkipAny creates a rule for skipping any symbol
func (p *Parser) SkipAny() error {
	if p.resultType != nil {
		return fmt.Errorf("can't add rules to already compiled parser")
	}

	i := instruction{
		op: opSkipAny,
	}
	p.program = append(p.program, i)
	return nil
}

// Compile compiles a reflection type for parsing results
func (p *Parser) Compile() error {
	resultFields := make([]reflect.StructField, 0)
	for _, fdesc := range p.fields {
		tag := reflect.StructTag(fmt.Sprintf(`json:"%s"`, fdesc.fieldName))
		fname := strings.Title(fdesc.fieldName)
		ftype := fdesc.fieldType
		resultFields = append(resultFields, reflect.StructField{
			Name: fname,
			Tag:  tag,
			Type: ftype,
		})
	}
	p.resultType = reflect.StructOf(resultFields)
	return nil
}

// ParseLine parses a string and returns a result struct of precompiled type
func (p *Parser) ParseLine(input string) (interface{}, error) {
	if p.resultType == nil {
		return nil, fmt.Errorf("can't run ParseLine before Compile")
	}
	line := []rune(input)
	linePointer := 0
	fieldIndex := 0
	result := reflect.New(p.resultType)
	for rIndex := 0; rIndex < len(p.program); rIndex++ {
		rule := p.program[rIndex]
		switch rule.op {
		case opSkip:
			if linePointer >= len(line) {
				return nil, fmt.Errorf("unexpected end of line")
			}
			if line[linePointer] != rule.arg1 {
				return nil, fmt.Errorf(
					"invalid symbol at pos %d: expected \"%s\"(%v), got \"%s\"(%v)",
					linePointer,
					string(rule.arg1),
					rule.arg1,
					string(line[linePointer]),
					line[linePointer],
				)
			}
			linePointer++
		case opSkipAny:
			if linePointer >= len(line) {
				return nil, fmt.Errorf("unexpected end of line")
			}
			linePointer++
		case opSkipTo:
			for {
				if linePointer >= len(line) {
					return nil, fmt.Errorf("unexpected end of line")
				}
				if line[linePointer] == rule.arg1 {
					break
				}
				linePointer++
			}
		case opUpTo:
			start := linePointer
			for {
				if linePointer >= len(line) {
					return nil, fmt.Errorf("unexpected end of line")
				}
				if line[linePointer] == rule.arg1 {
					break
				}
				linePointer++
			}
			opResult := string(line[start:linePointer])
			f := result.Elem().Field(fieldIndex)
			err := setData(f, opResult, p.fields[fieldIndex])
			if err != nil {
				return nil, err
			}
			fieldIndex++

		case opFromTo:
			if linePointer >= len(line) {
				return nil, fmt.Errorf("unexpected end of line")
			}
			if line[linePointer] != rule.arg1 {
				return nil, fmt.Errorf(
					"invalid symbol at pos %d: expected \"%s\"(%v), got \"%s\"(%v)",
					linePointer,
					string(rule.arg1),
					rule.arg1,
					string(line[linePointer]),
					line[linePointer],
				)
			}
			linePointer++

			start := linePointer
			for {
				if linePointer >= len(line) {
					return nil, fmt.Errorf("unexpected end of line")
				}
				if line[linePointer] == rule.arg2 {
					break
				}
				linePointer++
			}
			opResult := string(line[start:linePointer])
			f := result.Elem().Field(fieldIndex)
			err := setData(f, opResult, p.fields[fieldIndex])
			if err != nil {
				return nil, err
			}
			linePointer++
			fieldIndex++
		}
	}
	return result.Interface(), nil
}

func setData(f reflect.Value, strValue string, fdesc fieldDesc) error {
	if fdesc.conv == nil {
		f.SetString(strValue)
		return nil
	}

	return fdesc.conv(strValue, f)
}

func (p *Parser) optimize() {
	var rule instruction
	optimizedProg := make([]instruction, 0)
	i := 0

	for i < len(p.program) {
		rule = p.program[i]
		if rule.op == opSkip {
			if i+2 < len(p.program) {
				if p.program[i+1].op == opUpTo && p.program[i+2].op == opSkip {
					rule = instruction{
						op:   opFromTo,
						arg1: rule.arg1,
						arg2: p.program[i+1].arg1,
					}
					optimizedProg = append(optimizedProg, rule)
					i += 3
					continue
				}
			}
		}
		optimizedProg = append(optimizedProg, rule)
		i++
	}
	if len(optimizedProg) < len(p.program) {
		p.program = optimizedProg
	}
}

// FieldNames is a public getter
func (p *Parser) FieldNames() []string {
	names := make([]string, len(p.fields))
	for i, field := range p.fields {
		names[i] = field.fieldName
	}
	return names
}
