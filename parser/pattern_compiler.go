package parser

import (
	"fmt"
	"reflect"
	"regexp"
	"strings"
	"time"
)

type state int

const (
	stateReadSym state = iota
	stateReadVarname
	stateReadBracedVarname
)

var (
	variableSyms = regexp.MustCompile(`[\w-:\(\)%]`)
	// bracedVariableSyms = regexp.MustCompile(`[\w-:\(\)%\ ]`)

	variableName   = regexp.MustCompile(`^[\w-]+$`)
	datetimeFormat = regexp.MustCompile(`^dt\(([^\)]+)\)$`)
	rtab           = regexp.MustCompile(`(\t)`)
	rendl          = regexp.MustCompile(`(\n)`)
)

// FromPattern creates and configures a new parser according to a given line pattern
func FromPattern(pattern string) (*Parser, error) {
	var err error
	var sym rune
	var varName string

	if !strings.HasSuffix(pattern, "\n") {
		pattern += "\n"
	}

	p := NewParser()
	index := 0
	line := []rune(pattern)
	currentState := stateReadSym

	for index < len(line) {
		sym = line[index]
		index++

		switch currentState {
		case stateReadSym:
			switch sym {
			case '$':
				currentState = stateReadVarname
				varName = ""
			default:
				err = p.Skip(sym)
				if err != nil {
					return nil, err
				}
			}
		case stateReadVarname:
			ssym := string(sym)
			if varName == "" && sym == '{' {
				currentState = stateReadBracedVarname
			} else if variableSyms.MatchString(ssym) {
				varName += ssym
			} else {
				err = applyVar(varName, sym, p)
				if err != nil {
					return nil, err
				}
				currentState = stateReadSym
			}
		case stateReadBracedVarname:
			ssym := string(sym)
			if sym == '}' {
				// reading next symbol.
				// line can't end with }, we're safe from out of bounds panic
				sym = line[index]
				index++
				err = applyVar(varName, sym, p)
				if err != nil {
					return nil, err
				}
				currentState = stateReadSym
			} else {
				varName += ssym
			}

		}
	}

	if currentState != stateReadSym {
		return nil, fmt.Errorf("unexpected end of pattern")
	}

	p.optimize()
	p.Compile()

	return p, nil
}

func applyVar(varName string, sym rune, p *Parser) error {
	var err error

	if varName != "-" {
		fdesc, err := parseVariableExpression(varName)
		if err != nil {
			return err
		}
		err = p.UpTo(sym, fdesc.fieldName, fdesc.fieldType, fdesc.conv)
		if err != nil {
			return err
		}
		err = p.Skip(sym)
		if err != nil {
			return err
		}
	} else {
		err = p.SkipTo(sym)
		if err != nil {
			return err
		}
		err = p.Skip(sym)
		if err != nil {
			return err
		}
	}

	return nil
}

func parseVariableExpression(varExpression string) (*fieldDesc, error) {
	tokens := strings.SplitN(varExpression, ":", 2)
	if len(tokens) < 2 {
		// string variable
		return &fieldDesc{varExpression, reflect.TypeOf(""), nil}, nil
	}

	if !variableName.MatchString(tokens[0]) {
		return nil, fmt.Errorf("invalid variable name \"%s\"", tokens[0])
	}

	if match := datetimeFormat.FindStringSubmatch(tokens[1]); len(match) == 2 {
		convertDate := makeTimeConvert(match[1])
		return &fieldDesc{
			tokens[0],
			reflect.TypeOf(time.Now()),
			convertDate,
		}, nil
	}

	switch tokens[1] {
	case "str":
		return &fieldDesc{tokens[0], reflect.TypeOf(""), nil}, nil
	case "int":
		return &fieldDesc{tokens[0], reflect.TypeOf(int64(0)), convertInt}, nil
	case "uint":
		return &fieldDesc{tokens[0], reflect.TypeOf(uint64(0)), convertUint}, nil
	case "float":
		return &fieldDesc{tokens[0], reflect.TypeOf(float64(0)), convertFloat}, nil
	default:
		return nil, fmt.Errorf("invalid variable type \"%s\"", tokens[1])
	}
}
