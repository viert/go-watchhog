package config

import (
	"fmt"
	"io/ioutil"
	"strings"
	"time"

	"github.com/viert/go-watchhog/collector"
	"gopkg.in/yaml.v2"
)

const (
	defaultInterval = 60
	defaultBias     = 3
	defaultThreads  = 4
	defaultListen   = ":9009"
)

// WatcherConfig is a daemon configuration struct
type WatcherConfig struct {
	Listen     string
	Collectors map[string]*collector.Config
}

func unescape(input string) string {
	convertMap := map[string]string{
		`\\`: `\`,
		`\n`: "\n",
		`\t`: "\t",
		`\r`: "\r",
	}

	for from, to := range convertMap {
		input = strings.ReplaceAll(input, from, to)
	}
	return input
}

// Load creates a WatcherConfig, reading the given config file
func Load(filename string) (*WatcherConfig, error) {
	var wc WatcherConfig
	var i int

	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	err = yaml.Unmarshal(data, &wc)
	if err != nil {
		return nil, err
	}

	if wc.Listen == "" {
		wc.Listen = defaultListen
	}

	// check configuration
	i = 0
	for cName, cDesc := range wc.Collectors {
		cDesc.Name = cName
		if cDesc.Threads == 0 {
			cDesc.Threads = defaultThreads
		}
		if cDesc.Interval == 0 {
			cDesc.Interval = defaultInterval
		}
		if cDesc.Bias == 0 {
			cDesc.Bias = defaultBias
		}

		cDesc.Format = unescape(cDesc.Format)
		cDesc.Interval *= time.Second
		cDesc.Bias *= time.Second

		if cDesc.Prefix == "" {
			cDesc.Prefix = cDesc.Name
		}

		err = cDesc.ParseMetrics()
		if err != nil {
			return nil, err
		}
		i++
	}

	if i == 0 {
		return nil, fmt.Errorf("no collectors defined, giving up")
	}

	return &wc, nil
}
