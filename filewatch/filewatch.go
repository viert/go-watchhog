package filewatch

import (
	"fmt"
	"io"
	"os"
	"syscall"
)

// File is a io.ReadCloser interface on top of rotated files
type File struct {
	cf *os.File
}

// Open opens the given file and returns a wrapper object
func Open(filename string) (*File, error) {
	cf, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	// seeking to the end of file
	cf.Seek(0, 2)
	return &File{cf}, nil
}

// Read implements io.Reader interface
// This method checks if the underlying file was rotated every time it comes across io.EOF
// and tries to open the newly created file. If opening fails Read() doesn't throw errors
// either hoping that the new file will be created upon next read call
func (f *File) Read(buf []byte) (int, error) {
	n, err := f.cf.Read(buf)
	if err == nil {
		return n, err
	}

	if err == io.EOF {
		filename := f.cf.Name()

		newSt, err := os.Stat(filename)
		if err != nil {
			if os.IsNotExist(err) {
				// no new file found, that's ok, probably logrotate couldn't make it yet
				return 0, io.EOF
			}
			// something went wrong
			return 0, err
		}

		newStStat, ok := newSt.Sys().(*syscall.Stat_t)
		if !ok {
			return 0, fmt.Errorf("filewatcher doesn't support your OS")
		}

		oldSt, err := f.cf.Stat()
		if err != nil {
			// something went wrong
			return 0, err
		}

		oldStStat, ok := oldSt.Sys().(*syscall.Stat_t)
		if !ok {
			return 0, fmt.Errorf("filewatcher doesn't support your OS")
		}

		if newStStat.Ino != oldStStat.Ino {
			f.cf.Close()
			cf, err := os.Open(filename)
			if err != nil {
				// something went wrong
				return 0, fmt.Errorf("can't open newly created file: %v", err)
			}
			f.cf = cf
			// recursion call to Read after the underlying file descriptor has been changed
			return f.Read(buf)
		}
	}
	return 0, err
}

// Close implements io.Closer. Does nothing besides closing the underlying os.File
func (f *File) Close() {
	f.cf.Close()
}
