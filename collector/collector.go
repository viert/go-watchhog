package collector

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"reflect"
	"sort"
	"strings"
	"sync"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"

	"github.com/viert/go-watchhog/stringslice"

	"github.com/viert/go-watchhog/filewatch"
	"github.com/viert/go-watchhog/parser"
)

// Collector represents a collector object
type Collector struct {
	cfg    *Config
	rwrap  *ReaderWrapper
	stop   chan bool
	data   []interface{}
	gauges []*prometheus.GaugeVec
	wg     *sync.WaitGroup
	dlock  *sync.RWMutex
}

type response struct {
	Data []interface{} `json:"data"`
}

// New creates a new collector
func New(cc *Config) (*Collector, error) {
	prs, err := parser.FromPattern(cc.Format)
	if err != nil {
		return nil, err
	}
	f, err := filewatch.Open(cc.Filename)
	if err != nil {
		return nil, err
	}
	rwrap := NewReaderWrapper(f, prs, int(cc.Threads))

	fieldNames := prs.FieldNames()

	gauges := make([]*prometheus.GaugeVec, 0)
	for _, mc := range cc.metrics {
		if mc.GroupByField != "" {
			if !stringslice.Contains(mc.GroupByField, fieldNames) {
				return nil, fmt.Errorf("invalid field \"%s\": field not found in parser format", mc.GroupByField)
			}
		}
		for _, label := range mc.Labels {
			if !stringslice.Contains(label, fieldNames) {
				return nil, fmt.Errorf("invalid label \"%s\": label not found in parser format", label)
			}
		}
		g := promauto.NewGaugeVec(prometheus.GaugeOpts{Namespace: cc.Prefix, Name: mc.Name}, mc.Labels)
		gauges = append(gauges, g)
	}

	if cc.SortBy != "" && !stringslice.Contains(cc.SortBy, fieldNames) {
		return nil, fmt.Errorf("invalid sort_by field \"%s\": field not found in parser format", cc.SortBy)
	}

	return &Collector{
		cfg:    cc,
		rwrap:  rwrap,
		stop:   make(chan bool),
		data:   []interface{}{},
		gauges: gauges,
		wg:     new(sync.WaitGroup),
		dlock:  new(sync.RWMutex),
	}, nil
}

// Start starts collector in bg
func (c *Collector) Start() {
	c.wg.Add(1)
	go c.run()
}

// Stop stops the collector
func (c *Collector) Stop() {
	c.stop <- true
	log.Printf("collector %s is stopping", c.cfg.Name)
	c.wg.Wait()
}

func (c *Collector) run() {
	defer c.wg.Done()
	if c.cfg.Bias > 0 {
		timeToStart := time.Duration(rand.Int63n(int64(c.cfg.Bias)))
		log.Printf("collector %s is sleeping for %v", c.cfg.Name, timeToStart)
		select {
		case <-time.After(timeToStart):
		case <-c.stop:
			return
		}
	}

	log.Printf("collector %s is starting", c.cfg.Name)
	ticker := time.NewTicker(c.cfg.Interval)

	for {
		select {
		case <-ticker.C:
			c.next()
		case <-c.stop:
			return
		}
	}
}

func (c *Collector) next() {
	log.Printf("collector %s loading data", c.cfg.Name)
	data := c.rwrap.ParseChunk()

	if c.cfg.SortBy != "" {
		log.Printf("collector %s sorting data", c.cfg.Name)
		Sort(data, c.cfg.SortBy)
	}

	for i, mc := range c.cfg.metrics {
		log.Printf("collector %s building metric %s", c.cfg.Name, mc.Name)
		gvec := c.gauges[i]
		err := c.buildGauge(data, mc, gvec)
		if err != nil {
			log.Printf("%v", err)
		}
	}

	c.dlock.Lock()
	c.data = data
	c.dlock.Unlock()
}

func (c *Collector) buildGauge(data []interface{}, mc *MetricConfig, gvec *prometheus.GaugeVec) error {
	var found bool
	var itemValue float64
	var strValue, groupKey string

	fieldNames := make([]string, len(mc.Labels))
	for i, label := range mc.Labels {
		fieldNames[i] = strings.Title(label)
	}

	gField := strings.Title(mc.GroupByField)
	labelMap := make(map[string]prometheus.Labels)

	groupMap := make(map[string]float64)
	intermediateMap := make(map[string][]float64)

	for _, item := range data {
		if gField != "" {
			// Getting field float64 value in advance, we're likely to be needing this in a moment
			itemFieldVal := reflect.ValueOf(item).Elem().FieldByName(gField)
			switch itemFieldVal.Kind() {
			case reflect.Invalid:
				return fmt.Errorf("field \"%s\" not found", mc.GroupByField)
			case reflect.Uint64:
				itemValue = float64(itemFieldVal.Uint())
			case reflect.Int64:
				itemValue = float64(itemFieldVal.Int())
			default:
				return fmt.Errorf("field \"%s\" is not numeric", mc.GroupByField)
			}
		}

		// data is being grouped by given number of labels. so the data structure
		// for grouping is a map with key composed from label keys and values
		groupKey = ""

		labels := make(prometheus.Labels)

		for i, fieldName := range fieldNames {
			value := reflect.ValueOf(item).Elem().FieldByName(fieldName)
			switch value.Kind() {
			case reflect.String:
				strValue = value.String()
			case reflect.Uint64:
				strValue = fmt.Sprintf("%d", value.Uint())
			case reflect.Int64:
				strValue = fmt.Sprintf("%d", value.Int())
			case reflect.Float64:
				strValue = fmt.Sprintf("%.f", value.Float())
			default:
				return fmt.Errorf("you found narnia")
			}
			labels[mc.Labels[i]] = strValue
			groupKey += fmt.Sprintf("%s:%s,", fieldName, strValue)
		}
		labelMap[groupKey] = labels

		// now that we have a float64 value and a key for grouping

		if _, found = groupMap[groupKey]; !found {
			switch mc.GroupMethod {
			case GTSum:
				groupMap[groupKey] = itemValue
			case GTCount:
				groupMap[groupKey] = 1
			case GTMax:
				fallthrough
			case GTMin:
				fallthrough
			case GTAverage:
				fallthrough
			case GTMedian:
				groupMap[groupKey] = 1
				intermediateMap[groupKey] = []float64{itemValue}
			}
		} else {
			switch mc.GroupMethod {
			case GTSum:
				groupMap[groupKey] += itemValue
			case GTCount:
				groupMap[groupKey]++
			case GTMax:
				fallthrough
			case GTMin:
				fallthrough
			case GTAverage:
				fallthrough
			case GTMedian:
				intermediateMap[groupKey] = append(intermediateMap[groupKey], itemValue)
			}
		}
	}

	switch mc.GroupMethod {
	case GTMedian:
		for key, intermediate := range intermediateMap {
			sort.Float64s(intermediate)
			l := len(intermediate)
			if l%2 == 0 {
				groupMap[key] = (intermediate[l/2-1] + intermediate[l/2]) / 2
			} else {
				groupMap[key] = intermediate[l/2]
			}
		}
	case GTAverage:
		for key, intermediate := range intermediateMap {
			var sum float64 = 0
			for _, value := range intermediate {
				sum += value
			}
			groupMap[key] = sum / float64(len(intermediate))
		}
	case GTMin:
		for key, intermediate := range intermediateMap {
			var min float64 = 0
			if len(intermediate) > 0 {
				min = intermediate[0]
			}
			for _, value := range intermediate {
				if value < min {
					min = value
				}
			}
			groupMap[key] = min
		}
	case GTMax:
		for key, intermediate := range intermediateMap {
			var max float64 = 0
			if len(intermediate) > 0 {
				max = intermediate[0]
			}
			for _, value := range intermediate {
				if value > max {
					max = value
				}
			}
			groupMap[key] = max
		}
	}

	gvec.Reset()
	for key, value := range groupMap {
		labels := labelMap[key]
		metric, err := gvec.GetMetricWith(labels)
		if err != nil {
			log.Printf("error getting metric: %v", err)
			continue
		}
		metric.Set(value)
	}
	return nil
}

// RawDataJSON returns current raw data in JSON format
func (c *Collector) RawDataJSON() ([]byte, error) {
	c.dlock.RLock()
	defer c.dlock.RUnlock()
	return json.Marshal(&response{Data: c.data})
}

// Name is a public getter of collector's name
func (c *Collector) Name() string {
	return c.cfg.Name
}
