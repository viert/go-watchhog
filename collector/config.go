package collector

import (
	"fmt"
	"strings"
	"time"
)

type metricConfigParseState int

const (
	mcpsReadOperation metricConfigParseState = iota
	mcpsReadArgKey
	mcpsReadArgValue
)

// MetricConfig is a struct with metric configuration data
type MetricConfig struct {
	Name         string
	Labels       []string
	GroupByField string
	GroupMethod  GroupingType
}

// Config is a struct with collector configuration data
type Config struct {
	Name          string
	Filename      string
	Interval      time.Duration
	Bias          time.Duration
	Format        string
	Threads       uint
	SortBy        string `yaml:"sort_by"`
	Prefix        string
	metrics       []*MetricConfig   `yaml:"_"` // private for avoiding direct yaml access
	MetricsConfig map[string]string `yaml:"metrics"`
}

// ParseMetrics applies text metric configuration and creates []MetricConfig slice
func (c *Config) ParseMetrics() error {
	c.metrics = make([]*MetricConfig, 0)
	for name, cfg := range c.MetricsConfig {
		mc, err := makeMetric(name, cfg)
		if err != nil {
			return err
		}
		c.metrics = append(c.metrics, mc)
	}
	return nil
}

// Metrics is a public getter
func (c *Config) Metrics() []*MetricConfig {
	return c.metrics
}

func makeMetric(name string, cfg string) (*MetricConfig, error) {
	var (
		i         int
		argNum    int
		sym       rune
		state     metricConfigParseState
		operation string
		key       string
		value     string
		line      []rune
		mc        *MetricConfig
	)

	mc = &MetricConfig{Name: name, Labels: []string{}}
	line = []rune(strings.TrimSpace(cfg))
	state = mcpsReadOperation

	for i = 0; i < len(line); i++ {
		sym = line[i]
		switch state {
		case mcpsReadOperation:
			if sym == '(' {
				operation = strings.TrimSpace(operation)
				switch operation {
				case "count":
					mc.GroupMethod = GTCount
				case "average":
					mc.GroupMethod = GTAverage
				case "median":
					mc.GroupMethod = GTMedian
				case "sum":
					mc.GroupMethod = GTSum
				case "min":
					mc.GroupMethod = GTMin
				case "max":
					mc.GroupMethod = GTMax
				default:
					return nil, fmt.Errorf("invalid operation \"%s\" for metric \"%s\"", operation, name)
				}
				key = ""
				argNum++
				state = mcpsReadArgKey
			} else {
				operation += string(sym)
			}
		case mcpsReadArgKey:
			if sym == '=' {
				key = strings.TrimSpace(key)
				value = ""
				state = mcpsReadArgValue
			} else {
				key += string(sym)
			}
		case mcpsReadArgValue:
			if sym == ')' || sym == ' ' {
				value = strings.TrimSpace(value)
				switch key {
				case "field":
					mc.GroupByField = value
				case "labels":
					labels := strings.Split(value, ",")
					for _, lbl := range labels {
						mc.Labels = append(mc.Labels, strings.TrimSpace(lbl))
					}
				default:
					return nil, fmt.Errorf("invalid argument \"%s\"(%d) for metric %s", key, argNum, name)
				}
				if sym == ' ' {
					argNum++
					key = ""
					state = mcpsReadArgKey
				} else {
					break
				}
			} else {
				value += string(sym)
			}
		}
	}

	if state != mcpsReadArgValue {
		return nil, fmt.Errorf("unexpected end of line at symbol %d: %s", i, cfg)
	}

	if mc.GroupMethod != GTCount && mc.GroupByField == "" {
		return nil, fmt.Errorf("operation %s requires a 'field' argument", operation)
	}

	return mc, nil
}
