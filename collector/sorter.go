package collector

import (
	"fmt"
	"reflect"
	"sort"
	"strings"
	"time"
)

type sorter struct {
	data      []interface{}
	fieldName string
}

func newSorter(data []interface{}, fieldName string) *sorter {
	return &sorter{
		data:      data,
		fieldName: fieldName,
	}
}

func (s sorter) Len() int {
	return len(s.data)
}

func (s sorter) Swap(i, j int) {
	s.data[i], s.data[j] = s.data[j], s.data[i]
}

func (s sorter) Less(i, j int) bool {
	a := s.data[i]
	b := s.data[j]

	aValue := reflect.ValueOf(a).Elem().FieldByName(s.fieldName)
	bValue := reflect.ValueOf(b).Elem().FieldByName(s.fieldName)
	switch aValue.Kind() {
	case reflect.String:
		return aValue.String() < bValue.String()
	case reflect.Uint64:
		return aValue.Uint() < bValue.Uint()
	case reflect.Int64:
		return aValue.Int() < bValue.Int()
	case reflect.Float64:
		return aValue.Float() < bValue.Float()
	case reflect.Struct:
		aTime := aValue.Interface().(time.Time)
		bTime := bValue.Interface().(time.Time)
		return bTime.Sub(aTime) > 0
	default:
		return false
	}
}

// Sort sorts data by a given fieldName
func Sort(data []interface{}, fieldName string) error {
	if len(data) == 0 {
		return nil
	}

	// field checking
	name := strings.Title(fieldName)
	vkind := reflect.ValueOf(data[0]).Elem().FieldByName(name).Kind()

	switch vkind {
	case reflect.String:
		fallthrough
	case reflect.Uint64:
		fallthrough
	case reflect.Int64:
		fallthrough
	case reflect.Float64:
		s := newSorter(data, name)
		sort.Sort(s)
		return nil
	case reflect.Struct:
		value := reflect.ValueOf(data[0]).Elem().FieldByName(name)
		if _, ok := value.Interface().(time.Time); ok {
			s := newSorter(data, name)
			sort.Sort(s)
			return nil
		}
	case reflect.Invalid:
		return fmt.Errorf("field \"%s\" doesn't exist", fieldName)
	}

	return fmt.Errorf("field \"%s\" type doesn't support sorting", fieldName)
}
