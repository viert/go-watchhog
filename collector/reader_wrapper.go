package collector

import (
	"bufio"
	"io"
	"log"
	"strings"
	"sync"

	"github.com/viert/go-watchhog/parser"
)

type worker struct {
	id       int
	p        *parser.Parser
	wg       *sync.WaitGroup
	textchan <-chan string
	datachan chan<- interface{}
}

func (w *worker) run() {
	var data interface{}
	var err error
	var line string

	for line = range w.textchan {
		data, err = w.p.ParseLine(line)
		if err != nil {
			log.Printf("invalid line '%s': %v\n", strings.TrimSpace(line), err)
			continue
		}
		w.datachan <- data
	}
	w.wg.Done()
}

// ReaderWrapper is a multi-threaded parser processing data read from the given Reader
type ReaderWrapper struct {
	rd      io.Reader
	prs     *parser.Parser
	threads int
}

// NewReaderWrapper creates a new ReaderWrapper on the top of a Reader with a given number of workers
func NewReaderWrapper(rd io.Reader, prs *parser.Parser, workers int) *ReaderWrapper {
	pr := &ReaderWrapper{
		rd,
		prs,
		workers,
	}
	return pr
}

// ParseChunk reads the reader until EOF and returns parsed data
func (rwrap *ReaderWrapper) ParseChunk() []interface{} {
	linechan := make(chan string, 1024)
	datachan := make(chan interface{}, 1024)
	results := make([]interface{}, 0)

	workerGroup := new(sync.WaitGroup)
	workerGroup.Add(rwrap.threads)
	workers := make([]*worker, rwrap.threads)
	for i := 0; i < rwrap.threads; i++ {
		workers[i] = &worker{i, rwrap.prs, workerGroup, linechan, datachan}
		go workers[i].run()
	}

	go func() {
		sc := bufio.NewScanner(rwrap.rd)
		for sc.Scan() {
			linechan <- sc.Text() + "\n"
		}
		// closing linechan makes all the workers stop as they iterate through the linechan range
		close(linechan)
	}()

	fetcherGroup := new(sync.WaitGroup)
	fetcherGroup.Add(1)
	go func() {
		for parsed := range datachan {
			// results are populated from this thread only, synchronized by datachan
			// we're safe from possible slice race conditions
			results = append(results, parsed)
		}
		fetcherGroup.Done()
	}()

	workerGroup.Wait()
	// as all workers are stopped now we're sure nobody is going to write into datachan
	// so it's safe to close it
	close(datachan)
	// the datachan is closed, so the goroutine above is going to break the range datachan loop
	// and finalize the waitgroup. Let's wait until it's finished
	fetcherGroup.Wait()

	return results
}
