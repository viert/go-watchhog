package collector

// GroupingType is a type of dataset grouping
type GroupingType int

// GroupingType enum
const (
	GTSum GroupingType = iota
	GTCount
	GTAverage
	GTMedian
	GTMax
	GTMin
)
