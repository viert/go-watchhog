module github.com/viert/go-watchhog

go 1.13

require (
	github.com/gorilla/mux v1.7.3
	github.com/prometheus/client_golang v1.2.1
	github.com/viert/properties v0.0.0-20190120163359-e72631698e82
	gopkg.in/yaml.v2 v2.2.4
)
