package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"time"

	"github.com/viert/go-watchhog/collector"
	"github.com/viert/go-watchhog/parser"

	"github.com/viert/go-watchhog/config"
)

const (
	defaultConfigFilename = "/etc/watchhog.yml"
)

func main() {
	sorted := false
	logFilename := ""
	configFilename := ""
	collectorName := ""
	flag.StringVar(&configFilename, "c", defaultConfigFilename, "configuration file name")
	flag.StringVar(&logFilename, "f", "", "log file name")
	flag.StringVar(&collectorName, "n", "", "collector name")
	flag.Parse()

	if collectorName == "" {
		fmt.Fprintln(os.Stderr, "no collector name defined")
		os.Exit(1)
	}
	if logFilename == "" {
		fmt.Fprintln(os.Stderr, "no log filename defined")
		os.Exit(1)
	}

	wc, err := config.Load(configFilename)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error reading configuration: %v\n", err)
		os.Exit(3)
	}

	rd, err := os.Open(logFilename)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error opening log file: %v\n", err)
		os.Exit(2)
	}
	defer rd.Close()

	cc, found := wc.Collectors[collectorName]
	if !found {
		fmt.Fprintf(os.Stderr, "collector %s not found\n", collectorName)
		os.Exit(4)
	}

	prs, err := parser.FromPattern(cc.Format)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error creating parser: %v\n", err)
		os.Exit(5)
	}

	rwrap := collector.NewReaderWrapper(rd, prs, 4)
	t1 := time.Now()
	data := rwrap.ParseChunk()
	t2 := time.Now()
	if cc.SortBy != "" {
		err := collector.Sort(data, cc.SortBy)
		if err != nil {
			fmt.Fprintf(os.Stderr, "error sorting data: %v\n", err)
		} else {
			sorted = true
		}
	}
	t3 := time.Now()

	for _, item := range data {
		j, err := json.Marshal(item)
		if err != nil {
			continue
		}
		fmt.Println(string(j))
	}

	fmt.Printf("%d lines parsed in %s\n", len(data), t2.Sub(t1))
	if sorted {
		fmt.Printf("%d lines sorted in %s\n", len(data), t3.Sub(t2))
	}
}
