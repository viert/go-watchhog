package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"

	"github.com/viert/go-watchhog/watchhog"
)

const (
	defaultConfigFilename = "/etc/watchhog.yml"
)

func main() {
	configFilename := ""
	flag.StringVar(&configFilename, "c", defaultConfigFilename, "configuration file name")
	flag.Parse()

	wh, err := watchhog.New(configFilename)
	if err != nil {
		fmt.Fprintf(os.Stderr, "initialization error: %v\n", err)
		os.Exit(1)
	}

	// kbd interrupt handling
	sigs := make(chan os.Signal)
	signal.Notify(sigs, os.Interrupt)
	defer signal.Reset()

	wh.Start()

	<-sigs
	wh.Stop()
}
