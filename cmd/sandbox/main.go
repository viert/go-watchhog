package main

import (
	"os"
	"os/signal"

	"github.com/viert/go-watchhog/watchhog"
)

func main() {
	wh, err := watchhog.New("watchhog.yml")
	if err != nil {
		panic(err)
	}
	wh.Start()

	sigs := make(chan os.Signal)
	signal.Notify(sigs, os.Interrupt)
	defer signal.Reset()
	<-sigs

	wh.Stop()
}
